/*
 * Copyright (C) 2019  Project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib;

import org.apache.logging.log4j.Logger;
import org.metafacture.biblio.marc21.MarcXmlHandler;
import org.metafacture.io.ObjectJavaIoWriter;
import org.metafacture.mangling.RecordIdChanger;
import org.metafacture.metamorph.Filter;
import org.metafacture.metamorph.Metamorph;
import org.metafacture.strings.StringReader;
import org.metafacture.xml.XmlDecoder;
import org.swissbib.linked.linkeddata.ESBulkEncoder;
import org.swissbib.linked.mangling.EntitySplitter;

import java.io.StringWriter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

class MfWorkflow {
    private StringReader stringReader;
    private final WriterFactory writerFactory = new WriterFactory();
    private Logger log;

    MfWorkflow(Properties props, Logger logParam) {
        stringReader = createStringReader(props);
        this.log = logParam;
    }

    List<String> read(String input) {
        try {
            stringReader.process(input);
            String res = writerFactory.writer.toString();
            stringReader.resetStream();
            return Arrays.asList(res.split("\n\n"));
        } catch (Exception ex) {
            log.error(ex.getMessage());
            log.error(input);
            return Collections.emptyList();
        }
    }

    private StringReader createStringReader(Properties configProps) {
        final StringReader stringReader = new StringReader();
        final XmlDecoder xmlDecoder = new XmlDecoder();
        final MarcXmlHandler marcXmlHandler = new MarcXmlHandler();
        final Filter filter = new Filter(configProps.getProperty("morph.filter.path"));
        final Metamorph metamorph = new Metamorph(configProps.getProperty("morph.path"));
        final RecordIdChanger recordIdChanger = new RecordIdChanger();
        final ESBulkEncoder esBulkEncoder = new ESBulkEncoder();
        esBulkEncoder.setEscapeChars("true");
        esBulkEncoder.setHeader("false");
        esBulkEncoder.setIndex("");
        final ObjectJavaIoWriter<String> objectJavaIoWriter = new ObjectJavaIoWriter<>(writerFactory);

        stringReader.setReceiver(xmlDecoder)
                .setReceiver(marcXmlHandler)
                .setReceiver(filter)
                .setReceiver(metamorph);
        switch (configProps.getProperty("category")) {
            case "resource":
            case "document":
                metamorph.setReceiver(recordIdChanger);
                break;
            case "item":
            case "person":
            case "organisation":
                final EntitySplitter entitySplitter = new EntitySplitter();
                metamorph.setReceiver(entitySplitter)
                        .setReceiver(recordIdChanger);
                break;
        }
        recordIdChanger
                .setReceiver(esBulkEncoder)
                .setReceiver(objectJavaIoWriter);
        return stringReader;
    }
}
