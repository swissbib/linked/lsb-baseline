/*
 * Copyright (C) 2019  Project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.swissbib.types.CbsActions;
import org.swissbib.types.EsBulkActions;

import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class TopologyBuilder {
    private static final Logger logger = LogManager.getLogger();

    static Topology build(Properties props) {
        final Pattern pattern = Pattern.compile(".*\"@id\":\"([^\"]+)\".*");
        final MfWorkflow mfWorkflow = new MfWorkflow(props, logger);

        final String index = props.getProperty("es.index.name");

        final StreamsBuilder builder = new StreamsBuilder();
        KStream<String, SbMetadataModel> source = builder.stream(props.getProperty("kafka.topic.in"));
        KStream<String, SbMetadataModel> filtered = source.filter((k, v) -> {
            if (v.getCbsAction() == null) {
                logger.error("Document with id {} has no status message. Discarding message!", k);
                return false;
            }
            return true;
        });
        @SuppressWarnings("unchecked") KStream<String, SbMetadataModel>[] branches = filtered.branch(
                (k, v) -> v.getCbsAction() == CbsActions.REPLACE || v.getCbsAction() == CbsActions.CREATE,
                (k, v) -> v.getCbsAction() == CbsActions.DELETE
        );
        branches[0].mapValues(SbMetadataModel::getData)
                .flatMapValues(mfWorkflow::read)
                .filter((k, v) -> v != null && !v.isEmpty())
                .map((k, v) -> KeyValue.pair(
                        getId(pattern, v),
                        createEsModel(EsBulkActions.INDEX, v, index))
                )
                .to(props.getProperty("kafka.topic.out"));

        branches[1].mapValues(SbMetadataModel::getData)
                .flatMapValues(mfWorkflow::read)
                .filter((k, v) -> v != null && !v.isEmpty())
                .map((k, v) -> KeyValue.pair(
                        getId(pattern, v),
                        createEsModel(EsBulkActions.DELETE, v, index))
                )
                .to(props.getProperty("kafka.topic.out"));

        return builder.build();
    }

    private static String getId(Pattern pattern, String value) {
        Matcher matcher = pattern.matcher(value);
        if (matcher.find()) {
            return matcher.group(1).split("/")[4];
        }
        return null;
    }

    private static SbMetadataModel createEsModel(EsBulkActions action, String value, String index) {
        return new SbMetadataModel()
                .setEsBulkAction(action)
                .setData(value)
                .setEsIndexName(index);
    }
}
