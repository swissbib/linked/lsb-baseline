package org.swissbib;

import org.metafacture.io.IoWriterFactory;

import java.io.StringWriter;
import java.io.Writer;

public class WriterFactory implements IoWriterFactory {

    public StringWriter writer;

    @Override
    public Writer createWriter() {
        writer = new StringWriter();
        return writer;
    }
}