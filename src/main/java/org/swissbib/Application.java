/*
 * Copyright (C) 2019  Project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Application {
    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        SbProperties props = new SbProperties("configs/config.properties");
        Properties configProps = props.getAppProps();

        try (KafkaStreams filterStreams = new KafkaStreams(TopologyBuilder.build(configProps), props.buildKafkaProps())) {
            filterStreams.start();
            while (true) {
                if (!filterStreams.state().isRunning()) {
                    break;
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        System.exit(1);
    }
}
