/*
 * Copyright (C) 2019  Project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

final class SbProperties {
    private static Properties props;

    SbProperties(String propertiesJarPath) {
        SbProperties.props = new Properties();
        try {
            InputStream configStream = new FileInputStream("/app/config/config.properties");
            props.load(configStream);
        } catch (IOException e) {
            System.out.println("Can't find properties file in /app/config/!");
            try {
                props.load(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(propertiesJarPath)));
            } catch (IOException ex) {
                System.out.println("Can't find properties file in config.properties");
                System.exit(1);
            }
        }
    }

    Properties getAppProps() {
        return props;
    }

    Properties buildKafkaProps() {
        Properties kafkaProps = new Properties();
        kafkaProps.put(StreamsConfig.APPLICATION_ID_CONFIG, props.getProperty("kafka.application.id"));
        kafkaProps.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG,
                props.getProperty("kafka.broker.host") + ":" + props.getProperty("kafka.broker.port"));
        kafkaProps.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        kafkaProps.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, SbMetadataSerde.class);
        return kafkaProps;
    }
}
