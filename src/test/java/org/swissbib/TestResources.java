package org.swissbib;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.swissbib.types.CbsActions;
import org.swissbib.types.EsBulkActions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TestResources {
    private static final Logger logger = LogManager.getLogger();

    private static final SbProperties props = new SbProperties("configs/config.resource.properties");
    private static final TopologyTestDriver driver = new TopologyTestDriver(TopologyBuilder.build(props.getAppProps()), props.buildKafkaProps());


    private String loadData(String file_name) {
        String path = "src/test/resources/data/resourceTests";
        try {
            return new String(Files.readAllBytes(Paths.get(path + "/" + file_name)));
        } catch (IOException ex) {
            System.out.println("Could not find / read file " + file_name);
            return "";
        }
    }

    private void writeTestData(String fileName, String data) {
        String path = "src/test/resources/data";
        File file = new File(path + "/" + fileName);
        try {
            BufferedWriter stream = new BufferedWriter(new FileWriter(file));
            stream.write(data);
            stream.close();
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
    }

    @Test
    void testMetafacturePipe1() {
        ConsumerRecordFactory<String, SbMetadataModel> factory = new ConsumerRecordFactory<>(new StringSerializer(), new SbMetadataSerializer());
        driver.pipeInput(factory.create("sb-all", null, new SbMetadataModel().setData(loadData("test1.xml")).setCbsAction(CbsActions.CREATE)));

        ProducerRecord<String, SbMetadataModel> record = driver.readOutput("sb-resource-sink", new StringDeserializer(), new SbMetadataDeserializer());

        assertAll("resource test case 1",
                () -> assertEquals("301033935", record.key()),
                () -> assertEquals(loadData("output1.json"), record.value().getData())
        );
    }

    @Test
    void testMetafacturePipe2() {
        ConsumerRecordFactory<String, SbMetadataModel> factory = new ConsumerRecordFactory<>(new StringSerializer(), new SbMetadataSerializer());
        driver.pipeInput(factory.create("sb-all", null, new SbMetadataModel().setData(loadData("test2.xml")).setCbsAction(CbsActions.CREATE)));

        ProducerRecord<String, SbMetadataModel> record = driver.readOutput("sb-resource-sink", new StringDeserializer(), new SbMetadataDeserializer());
        assertAll("resource test case 2",
                () -> assertEquals("304307408", record.key()),
                () -> assertEquals(loadData("output2.json"), record.value().getData())
        );
    }

    @Test
    void testMetafacturePipe3() {
        ConsumerRecordFactory<String, SbMetadataModel> factory = new ConsumerRecordFactory<>(new StringSerializer(), new SbMetadataSerializer());
        driver.pipeInput(factory.create("sb-all", null, new SbMetadataModel().setData(loadData("test3.xml")).setCbsAction(CbsActions.CREATE)));

        ProducerRecord<String, SbMetadataModel> record = driver.readOutput("sb-resource-sink", new StringDeserializer(), new SbMetadataDeserializer());
        assertAll("resource test case 3",
                () -> assertEquals("000010960", record.key()),
                () -> assertEquals(loadData("output3.json"), record.value().getData())
        );
    }

    @Test
    void testMetafacturePipe4() {
        ConsumerRecordFactory<String, SbMetadataModel> factory = new ConsumerRecordFactory<>(new StringSerializer(), new SbMetadataSerializer());
        driver.pipeInput(factory.create("sb-all", null, new SbMetadataModel().setData(loadData("test4.xml")).setCbsAction(CbsActions.CREATE)));

        ProducerRecord<String, SbMetadataModel> record = driver.readOutput("sb-resource-sink", new StringDeserializer(), new SbMetadataDeserializer());
        assertAll("resource test case 4",
                () -> assertEquals("506712656", record.key()),
                () -> assertEquals(loadData("output4.json"), record.value().getData())
        );
    }

    @Test
    void testMetafacturePipe5() {
        ConsumerRecordFactory<String, SbMetadataModel> factory = new ConsumerRecordFactory<>(new StringSerializer(), new SbMetadataSerializer());
        driver.pipeInput(factory.create("sb-all", null, new SbMetadataModel().setData(loadData("test5.xml")).setCbsAction(CbsActions.CREATE)));

        ProducerRecord<String, SbMetadataModel> record = driver.readOutput("sb-resource-sink", new StringDeserializer(), new SbMetadataDeserializer());
        assertAll("resource test case 5",
                () -> assertEquals("506646440", record.key()),
                () -> assertEquals(loadData("output5.json"), record.value().getData())
        );
    }

    @Test
    void testMetafacturePipe6() {
        ConsumerRecordFactory<String, SbMetadataModel> factory = new ConsumerRecordFactory<>(new StringSerializer(), new SbMetadataSerializer());
        driver.pipeInput(factory.create("sb-all", null, new SbMetadataModel().setData(loadData("test6.xml")).setCbsAction(CbsActions.CREATE)));

        ProducerRecord<String, SbMetadataModel> record = driver.readOutput("sb-resource-sink", new StringDeserializer(), new SbMetadataDeserializer());
        assertAll("resource test case 6",
                () -> assertEquals("338415165", record.key()),
                () -> assertEquals(loadData("output6.json"), record.value().getData())
        );
    }

    @Test
    void testMetafacturePipe7() {
        ConsumerRecordFactory<String, SbMetadataModel> factory = new ConsumerRecordFactory<>(new StringSerializer(), new SbMetadataSerializer());
        driver.pipeInput(factory.create("sb-all", null, new SbMetadataModel().setData(loadData("test7.xml")).setCbsAction(CbsActions.CREATE)));

        ProducerRecord<String, SbMetadataModel> record = driver.readOutput("sb-resource-sink", new StringDeserializer(), new SbMetadataDeserializer());
        assertAll("resource test case 7",
                () -> assertEquals("30375897X", record.key()),
                () -> assertEquals(loadData("output7.json"), record.value().getData())
        );
    }

    @Test
    void testMetafacturePipe8() {
        ConsumerRecordFactory<String, SbMetadataModel> factory = new ConsumerRecordFactory<>(new StringSerializer(), new SbMetadataSerializer());
        driver.pipeInput(factory.create("sb-all", null, new SbMetadataModel().setData(loadData("test8.xml")).setCbsAction(CbsActions.REPLACE)));

        ProducerRecord<String, SbMetadataModel> record = driver.readOutput("sb-resource-sink", new StringDeserializer(), new SbMetadataDeserializer());

        assertAll("resource test case 8",
                () -> assertEquals("264662156", record.key()),
                () -> assertEquals(EsBulkActions.INDEX, record.value().getEsBulkAction()),
                () -> assertEquals("resource", record.value().getEsIndexName()),
                () -> assertEquals(loadData("output8.json"), record.value().getData())
        );
    }

    @Test
    void testMetafacturePipe9() {
        ConsumerRecordFactory<String, SbMetadataModel> factory = new ConsumerRecordFactory<>(new StringSerializer(), new SbMetadataSerializer());
        driver.pipeInput(factory.create("sb-all", null, new SbMetadataModel().setData(loadData("test9.xml")).setCbsAction(CbsActions.REPLACE)));

        ProducerRecord<String, SbMetadataModel> record = driver.readOutput("sb-resource-sink", new StringDeserializer(), new SbMetadataDeserializer());

        assertAll("resource test case 8",
                () -> assertEquals("128896086", record.key()),
                () -> assertEquals(EsBulkActions.INDEX, record.value().getEsBulkAction()),
                () -> assertEquals("resource", record.value().getEsIndexName()),
                () -> assertEquals(loadData("output9.json"), record.value().getData())
        );
    }


    @AfterAll
    static void close() {
        driver.close();
    }

}
