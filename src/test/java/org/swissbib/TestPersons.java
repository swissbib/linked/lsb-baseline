package org.swissbib;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.swissbib.types.CbsActions;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TestPersons {
    private static final Logger logger = LogManager.getLogger();

    private static final SbProperties props = new SbProperties("configs/config.person.properties");
    private static final TopologyTestDriver driver = new TopologyTestDriver(TopologyBuilder.build(props.getAppProps()), props.buildKafkaProps());


    private String loadData(String file_name) {
        String path = "src/test/resources/data/personTests";
        try {
            return new String(Files.readAllBytes(Paths.get(path + "/" + file_name)));
        } catch (IOException ex) {
            System.out.println("Could not find / read file " + file_name);
            return "";
        }
    }

    @Test
    void testMetafacturePipe1() {
        ConsumerRecordFactory<String, SbMetadataModel> factory = new ConsumerRecordFactory<>(new StringSerializer(), new SbMetadataSerializer());
        driver.pipeInput(factory.create("sb-all", null, new SbMetadataModel().setData(loadData("test1.xml")).setCbsAction(CbsActions.CREATE)));

        ProducerRecord<String, SbMetadataModel> record = driver.readOutput("sb-linker-source", new StringDeserializer(), new SbMetadataDeserializer());
        ProducerRecord<String, SbMetadataModel> record2 = driver.readOutput("sb-linker-source", new StringDeserializer(), new SbMetadataDeserializer());
        ProducerRecord<String, SbMetadataModel> record3 = driver.readOutput("sb-linker-source", new StringDeserializer(), new SbMetadataDeserializer());
        ProducerRecord<String, SbMetadataModel> record4 = driver.readOutput("sb-linker-source", new StringDeserializer(), new SbMetadataDeserializer());

        assertAll("ensure non null values!",
                () -> assertNotNull(record),
                () -> assertNotNull(record2),
                () -> assertNotNull(record3),
                () -> assertNull(record4)
        );

        assertAll("person test case 1",
                () -> assertEquals("9bd9f471-5663-34d1-81de-25d1dabaf868", record.key()),
                () -> assertEquals(loadData("output1.1.json"), record.value().getData()),
                () -> assertEquals("f961e907-143a-3aa8-b3fd-142eb9e02fef", record2.key()),
                () -> assertEquals(loadData("output1.2.json"), record2.value().getData()),
                () -> assertEquals("b5b47882-3109-3a61-aa60-aeb37467672f", record3.key()),
                () -> assertEquals(loadData("output1.3.json"), record3.value().getData())
        );
    }

    @Test
    void testMetafacturePipe2() {
        ConsumerRecordFactory<String, SbMetadataModel> factory = new ConsumerRecordFactory<>(new StringSerializer(), new SbMetadataSerializer());
        driver.pipeInput(factory.create("sb-all", null, new SbMetadataModel().setData(loadData("test2.xml")).setCbsAction(CbsActions.CREATE)));

        ProducerRecord<String, SbMetadataModel> record = driver.readOutput("sb-linker-source", new StringDeserializer(), new SbMetadataDeserializer());
        ProducerRecord<String, SbMetadataModel> record2 = driver.readOutput("sb-linker-source", new StringDeserializer(), new SbMetadataDeserializer());
        ProducerRecord<String, SbMetadataModel> record3 = driver.readOutput("sb-linker-source", new StringDeserializer(), new SbMetadataDeserializer());

        assertAll("ensure non null values!",
                () -> assertNotNull(record),
                () -> assertNotNull(record2),
                () -> assertNull(record3)
        );

        assertAll("person test case 2",
                () -> assertEquals("fcc7b3cc-eb27-35a6-94d0-97e75897e812", record.key()),
                () -> assertEquals(loadData("output2.1.json"), record.value().getData()),
                () -> assertEquals("b5b47882-3109-3a61-aa60-aeb37467672f", record2.key()),
                () -> assertEquals(loadData("output2.2.json"), record2.value().getData())
        );
    }

    @Test
    void testMetafacturePipe3() {
        MfWorkflow workflow = new MfWorkflow(props.getAppProps(), logger);
        List<String> results = workflow.read(loadData("test3.xml"));

        assertEquals(1, results.size());
        assertEquals(loadData("output3.1.json"), results.get(0));
    }

    @Test
    void testMetafacturePipe4() {
        MfWorkflow workflow = new MfWorkflow(props.getAppProps(), logger);
        List<String> results = workflow.read(loadData("test4.xml"));

        assertEquals(2, results.size());
        assertEquals(loadData("output4.1.json"), results.get(0));
        assertEquals(loadData("output4.2.json"), results.get(1));
    }

    @Test
    void testMetafacturePipe5() {
        MfWorkflow workflow = new MfWorkflow(props.getAppProps(), logger);
        List<String> results = workflow.read(loadData("test5.xml"));

        assertEquals(2, results.size());
        assertEquals(loadData("output5.1.json"), results.get(0));
        assertEquals(loadData("output5.2.json"), results.get(1));
    }


    @Test
    void testMetafacturePipe6() {
        MfWorkflow workflow = new MfWorkflow(props.getAppProps(), logger);
        List<String> results = workflow.read(loadData("test6.xml"));

        assertEquals(1, results.size());
        assertEquals(loadData("output6.1.json"), results.get(0));
    }

    @Test
    void testMetafacturePipe7() {
        MfWorkflow workflow = new MfWorkflow(props.getAppProps(), logger);
        List<String> results = workflow.read(loadData("test7.xml"));

        assertEquals(1, results.size());
        assertEquals(loadData("output7.1.json"), results.get(0));
    }


    @Test
    void testMetafacturePipe9() {
        ConsumerRecordFactory<String, SbMetadataModel> factory = new ConsumerRecordFactory<>(new StringSerializer(), new SbMetadataSerializer());
        driver.pipeInput(factory.create("sb-all", null, new SbMetadataModel().setData(loadData("test9.xml")).setCbsAction(CbsActions.CREATE)));

        ProducerRecord<String, SbMetadataModel> record = driver.readOutput("sb-linker-source", new StringDeserializer(), new SbMetadataDeserializer());
        ProducerRecord<String, SbMetadataModel> record2 = driver.readOutput("sb-linker-source", new StringDeserializer(), new SbMetadataDeserializer());
        assertAll("ensure non null values!",
                () -> assertNotNull(record),
                () -> assertNull(record2)
        );

        assertAll("person test case 9",
                () -> assertEquals("9708e798-fd9b-3ab0-8e8f-93b57604e7d8", record.key()),
                () -> assertEquals(loadData("output9.1.json"), record.value().getData())
        );
    }
}
