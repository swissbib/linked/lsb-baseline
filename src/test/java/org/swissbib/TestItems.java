package org.swissbib;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TestItems {
    private static final Logger logger = LogManager.getLogger();

    private final SbProperties props = new SbProperties("configs/config.item.properties");

    private String loadData(String file_name) {
        String path = "src/test/resources/data";
        try {
            return new String(Files.readAllBytes(Paths.get(path + "/" + file_name)));
        } catch (IOException ex) {
            System.out.println("Could not find / read file " + file_name);
            return "";
        }
    }

    private void writeTestData(String fileName, String data) {
        String path = "src/test/resources/data";
        File file = new File(path + "/" + fileName);
        try {
            BufferedWriter stream = new BufferedWriter(new FileWriter(file));
            stream.write(data);
            stream.close();
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
    }

    @Test
    void testMetafacturePipe1() {
        MfWorkflow workflow = new MfWorkflow(props.getAppProps(), logger);
        List<String> results = workflow.read(loadData("itemTest1/itemTest1.xml"));

        assertEquals(3, results.size());

        for (int i = 0; i < 3; i++) {

            writeTestData("itemTest1/itemTestOut1/itemTestOut1." + i + ".json", results.get(i));
            assertEquals(loadData("itemTest1/itemTestOut1/itemTestOut1." + i + ".json"), results.get(i));

        }
    }

}
