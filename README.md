# lsb-baseline

Provides the Kafka Streams applications for splitting a holistic MARC-XML
document into entities of up to five categories. Because the workflows for
the different categories are nearly identical apart from the mapping, the
respective services are based on the same code, but different Dockerfiles
are used to build the images.

## Build

As mentioned before, the code contains five distinctive pipelines. It is
intended that they are used as containerized microservices, so you 
normally want to build them as Docker images. For this purpose there are
different Dockerfiles available:

* `Dockerfile.document`
* `Dockerfile.item`
* `Dockerfile.organisation`
* `Dockerfile.person`
* `Dockerfile.resource`

Build an image by issuing the following command in the root directory:

```bash
docker build -t<name_of_dockerimage> -f<name_of_dockerfile> .
```

## Custom Configuration

The individual default configuration for a pipeline consists of the properties
set in `src/main/resources/config.all.properties` as well as the properties
found in the specific configuration file (e.g. 
`src/main/resources/config.organisation.properties`). So if you want to
override the default configuration, start by concatenating the two files.
The final file can be mounted on `/app/config/config.properties` in the
container filesystem.